import React, { Component } from "react";

export default class SelectLabel extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { labels, currentLabel } = this.props;
    return (
      <div>
        {labels.map(label => {
          let classes = `btn m-2 btn-label `;
          classes += currentLabel === label ? 'btn-primary' : 'btn-light';
          return (
            <button key={label} onClick={() => this.props.onUpdate({label})} type="button" className={classes}>{label}</button>
          );
        })}
      </div>
    );
  }
}

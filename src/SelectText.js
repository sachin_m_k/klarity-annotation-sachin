import React, { Component } from 'react';

export default class SelectText extends Component {
    constructor(props){
        super(props);
        this.state = {

        }
        this.itemRef = React.createRef();
        this.selectionChangeTimer = null;
    }

    componentDidMount(){
        document.addEventListener('selectionchange', this.handleSelectionChange)
    }

    componentWillUnmount(){

    }

    createSelection = () => {
        const range = new Range();
        const selection = document.getSelection();
        const {prev_text: prevText, text} = this.props.item;
        const itemTextNode = this.itemRef.current.firstChild;
        range.setStart(itemTextNode, prevText.length);
        range.setEnd(itemTextNode, prevText.length + text.length);
        selection.removeAllRanges();
        selection.addRange(range);
    }

    handleSelectionChange = () => {
        clearTimeout(this.selectionChangeTimer);
        this.selectionChangeTimer = setTimeout(this.updateSelection, 500);

    }

    updateSelection = () => {
        const selection = document.getSelection();
        if (! selection.rangeCount || selection.isCollapsed) return;
        console.log(selection)
        console.log(selection.getRangeAt(0))
        const range = selection.getRangeAt(0);
        let newText = range.cloneContents().textContent;

        let newPrevRange = new Range();
        newPrevRange.setStart(this.itemRef.current.firstChild, 0);
        newPrevRange.setEnd(range.startContainer, range.startOffset);
        console.log(newPrevRange.cloneContents().textContent)
        let newPrevText = newPrevRange.cloneContents().textContent;
        let newNextRange = new Range();
        newNextRange.setStart(range.endContainer, range.endOffset);
        newNextRange.setEnd(this.itemRef.current.lastChild, this.itemRef.current.lastChild.length)
        console.log(newNextRange.cloneContents().textContent);
        let newNextText = newNextRange.cloneContents().textContent;
        this.props.onUpdateSelection({prev_text: newPrevText, text: newText, next_text: newNextText})

    }

    render(){
        const {item} = this.props;
        const allItemText = [item['prev_text'], item['text'], item['next_text']].join('');

        return (
            <div>
                <p ref={this.itemRef}>
                    {item['prev_text']}
                    <em>{item['text']}</em>
                    {item['next_text']}
                </p>
            </div>
        )
    }
}

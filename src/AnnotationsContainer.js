import React, { Component } from "react";
import Hotkeys from "react-hot-keys";
import { FaRegCopy } from "react-icons/fa";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Tooltip from "react-bootstrap/Tooltip";

import data from "./input.json";
import labels from "./labels.json";

import SelectText from "./SelectText";
import SelectLabel from "./SelectLabel";
import MyModal from "./Modal";

console.log(data.length);
export default class AnnotationsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [...data],
      labels: [...labels.list],
      currentIndex: 0,
      showModal: false
    };
  }

  handleCloseModal = () => {
    this.setState({
      showModal: false
    });
  };

  handleOpenModal = () => {
    this.setState({
      showModal: true
    });
  };
  handleNextItem = () => {
    let { currentIndex } = this.state;
    currentIndex += currentIndex + 1 >= this.state.data.length ? 0 : 1;
    console.log(currentIndex, this.state.data.length);
    this.setState({
      currentIndex
    });
  };

  handlePrevItem = () => {
    let { currentIndex } = this.state;
    currentIndex -= currentIndex - 1 < 0 ? 0 : 1;
    console.log(currentIndex, this.state.data[currentIndex]);
    this.setState({
      currentIndex
    });
  };

  handleUpdateSelection = payload => {
    let currentItemId = this.state.data[this.state.currentIndex]["Id"];
    let newData = this.state.data.map(item => {
      if (item["Id"] === currentItemId) {
        return {
          ...item,
          ...payload
        };
      }
      return item;
    });
    console.log(newData.find(item => item["Id"] === currentItemId));
    this.setState({
      data: newData
    });
  };

  handleOnCopy = (event) => {
    event.preventDefault();
    const currentItem = this.state.data[this.state.currentIndex];
    var elem = document.createElement("textarea");
    elem.value = currentItem["text"];
    document.body.appendChild(elem);
    // elem.focus();
    elem.select();
    document.execCommand("copy");
    document.body.removeChild(elem);
  };

  render() {
    const { currentIndex, labels, data } = this.state;
    const currentItem = data[currentIndex];
    return (
      <>
        <div className="row my-3">
          <div className="col no-select">
            <p>Use your mouse or keyboard to select text</p>
            <h6>Keyboard shortcuts </h6>
            <div>
              <kbd>Shift + left</kbd>
              <span> Previous item</span>
            </div>
            <div>
              <kbd>Shift + right</kbd>
              <span> Next item</span>
            </div>
            <div>
              <kbd>Shift + a</kbd>
              <span> Add label</span>
            </div>
          </div>
        </div>
        <hr />
        <MyModal
          header="Choose label"
          show={this.state.showModal}
          onHide={this.handleCloseModal}
        >
          <SelectLabel
            currentLabel={currentItem["label"]}
            key={currentItem["Id"]}
            labels={labels}
            onUpdate={payload => {
              this.handleCloseModal();
              this.handleUpdateSelection(payload);
            }}
          />
        </MyModal>
        <div className="row mt-5 d-flex justify-content-center">
          <div className="col-md-8 p-0 d-flex justify-content-between">
            <div className="flex-fill">
              <button
                style={{ width: "33%" }}
                disabled={currentIndex === 0}
                onClick={this.handlePrevItem}
                className="btn mx-0 btn-outline-dark no-select"
              >
                Previous
              </button>
              <button
                style={{ width: "33%" }}
                disabled={currentIndex === data.length - 1}
                onClick={this.handleNextItem}
                className="btn mx-4 btn-outline-dark no-select"
              >
                Next
              </button>
            </div>
            <div className="flex-fill d-flex justify-content-end">
              <OverlayTrigger
                placement="top"
                overlay={<Tooltip id={`tooltip-top`}>Copy highlighted text</Tooltip>}
              >
                <button
                  onClick={this.handleOnCopy}
                  className="btn mx-4 btn-outline-primary"
                >
                  <FaRegCopy />
                </button>
              </OverlayTrigger>
              <button
                style={{ width: "43%" }}
                disabled={currentItem["text"].length === 0}
                onClick={this.handleOpenModal}
                className="btn btn-primary no-select"
              >
                Add label
              </button>
            </div>
          </div>
        </div>
        <div className="row my-lg-3 justify-content-center">
          <div className="col-md-8 card">
            <div className="card-body">
              <pre className="no-select">Id: {currentItem["Id"]}</pre>
              <pre className="no-select">
                Label:{" "}
                {currentItem["label"] ? (
                  <span className="badge badge-primary">
                    {currentItem["label"]}
                  </span>
                ) : (
                  "Not defined"
                )}
              </pre>
              <pre className="no-select">
                Text length:{" "}
                {currentItem["text"].length +
                  currentItem["prev_text"].length +
                  currentItem["next_text"].length}
              </pre>
              <SelectText
                item={currentItem}
                onUpdateSelection={this.handleUpdateSelection}
              />
            </div>
          </div>
        </div>

        <div className="row my-lg-5">
          <div className="col card">
            <div className="card-body">
              <h4 className="no-select">Result</h4>
              <pre
                style={{ whiteSpace: "pre-wrap" }}
                className="my-2 no-select"
              >
                {JSON.stringify(currentItem, null, 2)}
              </pre>
            </div>
          </div>
        </div>
        <Hotkeys
          keyName="shift+right"
          onKeyDown={this.handleNextItem}
          // onKeyUp={this.onKeyUp}
        ></Hotkeys>
        <Hotkeys
          keyName="shift+left"
          onKeyDown={this.handlePrevItem}
          // onKeyUp={this.onKeyUp}
        ></Hotkeys>
        <Hotkeys
          keyName="shift+a"
          onKeyDown={this.handleOpenModal}
          // onKeyUp={this.onKeyUp}
        ></Hotkeys>
      </>
    );
  }
}

import React from "react";
import logo from "./logo.svg";
import "./App.css";
import AnnotationsContainer from "./AnnotationsContainer";


function App() {
  return (
    <div className="container">
        <AnnotationsContainer/>
    </div>
  );
}

export default App;
